const gulp = require('gulp')
const gulpPug = require('gulp-pug')
const gulpPlumber = require('gulp-plumber')
const gulpSass = require('gulp-sass')(require('sass'));
const gulpAutoprefixer = require('gulp-autoprefixer')
const cleanCSS = require('gulp-clean-css');
const sourcemaps = require('gulp-sourcemaps');
const babel = require('gulp-babel');
const uglify = require('gulp-uglify');
const gulpClean = require('gulp-clean');
const browserSync = require('browser-sync').create();
const gulpImagemin = require('gulp-imagemin');
const svgSprite = require('gulp-svg-sprite');
const svgmin = require('gulp-svgmin');
const cheerio = require('gulp-cheerio');
const replace = require('gulp-replace');


function pug2html() {
    return gulp.src('src/pug/pages/*.pug')
        .pipe(gulpPlumber())
        .pipe(gulpPug({
            pretty: true
        }))
        .pipe(gulpPlumber.stop())
        .pipe(gulp.dest('build'));
}

function sass2css() {
    return gulp.src('src/static/styles/styles.sass')
        .pipe(gulpPlumber())
        .pipe(gulpSass())
        .pipe(gulpAutoprefixer())
        .pipe(sourcemaps.init())
        .pipe(cleanCSS({level: 2}))
        .pipe(sourcemaps.write())
        .pipe(gulpPlumber.stop())
        .pipe(gulp.dest('build/static/css/'))
        .pipe(browserSync.stream());
}

function scripts() {
    return gulp.src('src/static/js/main.js')
        .pipe(sourcemaps.init())
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('build/static/js/'))
        .pipe(browserSync.stream());
}

function clean() {
    return gulp.src('build', {read: false})
        .pipe(gulpClean());
}

function imagemin() {
    return gulp.src([
        'src/static/img/**/*.{jpg, gif, png, svg, jpeg}',
        '!src/static/img/sprite/*'
    ])
        .pipe(gulpImagemin([
            gulpImagemin.gifsicle({interlaced: true}),
            gulpImagemin.mozjpeg({quality: 75, progressive: true}),
            gulpImagemin.optipng({optimizationLevel: 5}),
            gulpImagemin.svgo({
                plugins: [
                    {removeViewBox: true},
                    {cleanupIDs: false}
                ]
            })
        ]))
        .pipe(gulp.dest('build/static/img/'));
}

function svgSpriteBuild() {
    return gulp.src('src/static/img/sprite/*.svg')
        // minify svg
        .pipe(svgmin({
            js2svg: {
                pretty: true
            }
        }))
        // remove all fill, style and stroke declarations in out shapes
        .pipe(cheerio({
            run: function ($) {
                $('[fill]').removeAttr('fill');
                $('[stroke]').removeAttr('stroke');
                $('[style]').removeAttr('style');
            },
            parserOptions: {xmlMode: true}
        }))
        // cheerio plugin create unnecessary string '&gt;', so replace it.
        .pipe(replace('&gt;', '>'))
        // build svg sprite
        .pipe(svgSprite({
            mode: {
                symbol: {
                    sprite: "sprite.svg",
                }
            }
        }))
        .pipe(gulp.dest('build/static/img/sprite'));
}

function fonts() {
    return gulp.src('src/static/fonts/**/*.*')
        .pipe(gulp.dest('build/static/fonts/'))
        .pipe(browserSync.stream());
}

function watch() {
    browserSync.init({
        server: {
            baseDir: "build"
        }
    });
    gulp.watch('src/static/img/**/*.{jpg, gif, png, svg, jpeg}, !src/static/img/sprite/*', imagemin);
    gulp.watch('src/static/img/sprite/*', svgSpriteBuild);
    gulp.watch("src/pug/**/*.pug", pug2html);
    gulp.watch("src/static/styles/**/*.sass", sass2css);
    gulp.watch("src/static/js/**/*.js", scripts);
    gulp.watch("src/static/fonts/**/*.*", fonts)
    gulp.watch("build/*.html").on('change', browserSync.reload);
}


exports.default = gulp.series(clean, pug2html,  sass2css, scripts, svgSpriteBuild, imagemin, fonts, watch)